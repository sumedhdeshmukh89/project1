from selenium import webdriver
import time

driver = webdriver.Chrome()
driver.maximize_window()
driver.implicitly_wait(10)

driver.get('https://www.ataevents.org/')

# link1 = //*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/div/h3/a
# link1 = //*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/div/h3/a

link1 = driver.find_element_by_xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[1]/div/div/div/div/div/div/h3/a")
link1.click()
# pageTitle = driver.title
# print(pageTitle)



# link2 = //*[@id="post-18"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/div/h3/a

link2 = driver.find_element_by_xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/div/h3/a")
link2.click()
# pageTitle2 = driver.title
# print(pageTitle2)

listwindowhandle = driver.window_handles

for handle in listwindowhandle:
    print(handle)
    driver.switch_to.window(handle)
    print(driver.title)

# driver.close()
driver.quit()

# lst = driver.find_elements_by_xpath("//*[@id=\"post-18\"]/div/div/div/div/div/section[3]/div/div/div[2]/div/div/div/div/div/div/h3/a")

# for webpage in list
#     links = webpage.c