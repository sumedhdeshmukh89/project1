from selenium import webdriver
import time

driver = webdriver.Chrome()
driver.maximize_window()

driver.get('https://agiletestingalliance.org/')


# linkedin //*[@id="custom_html-10"]/div/ul/li[1]/a/i
# twitter //*[@id="custom_html-10"]/div/ul/li[2]/a/i
# all icons //*[@id="custom_html-10"]/div/ul/li/a

lst = driver.find_elements_by_xpath(" //*[@id=\"custom_html-10\"]/div/ul/li/a")

for webele in lst:
    address = webele.get_attribute("href")
    print(address)
