import openpyxl
import pytest
from selenium import webdriver
from Day5.CalcPOM import ataCalcPage
import time
import pandas as pd


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()

    driver.maximize_window()
    driver.implicitly_wait(10)

def dp_pandas(atacalcdata=df):
    xlpath = "C:/Users/sumedh_deshmukh/PycharmProjects/Practice/resources/data/atacalcdata.xlsx"
    df = pd.data(atacalcdata.xlsx)


def teardown():
    print('Teardonw Method()')
    time.sleep(3)
    driver.quit()

def dataprovider():
    # lol = [ [12,  13,  'Add',  25],
    #         [10,  20,  'Mul',  200],
    #         [18,  32,  'Add',  50],
    #         [500, 100, 'comp',  500],
    #         [50,  12,  'add',  62]   ]

    xlpath = "C:/Users/sumedh_deshmukh/PycharmProjects/Practice/resources/data/atacalcdata.xlsx"
    wb = openpyxl.load_workbook(xlpath)
    sh = wb['calcdata']
    nrow = sh.max_row
    print('No. of rows:', nrow)

    lol = []
    for i in range(2, nrow+1):
        lst = []
        v1  = sh.cell(i,1)
        v2  = sh.cell(i,2)
        v3  = sh.cell(i,3)
        v4  = sh.cell(i,4)
        v5  = sh.cell(i,5)
        v6 = sh.cell(i,6)
        lst.insert(0,v1.value)          #lst = [v1.value]
        lst.insert(1,v2.value)
        lst.insert(2,v3.value)
        lst.insert(3,v4.value)
        lst.insert(4,v5.value)
        lst.insert(5,v6.value)
        lol.insert(i-1,lst)

    return lol

    # return [[12,13,'Add',25]]


@pytest.mark.parametrize('inputdata', dataprovider())
def test_calc_all(inputdata):
    print(inputdata)
    input1 = inputdata[0]
    input2 = inputdata[1]
    operation = inputdata[2]
    expRes = inputdata[3]
    actRes = ''


    driver.get("http://ata123456789123456789.appspot.com/")
    atacalcpage = ataCalcPage(driver)

    if(operation.upper() == 'ADD'):
        actRes = atacalcpage.add(input1, input2)
    elif (operation.upper() == 'MUL'):
        actRes = atacalcpage.Mul(input1, input2)
    elif (operation.upper() == 'COMP'):
        actRes = atacalcpage.comp(input1, input2)
    elif (operation.upper() == 'EUC'):
        actRes = atacalcpage.euc(input1, input2)
    else:
        print('Invalid Data')

    print(expRes, actRes)
    assert actRes == expRes




# def test_Calc_add():
#     driver.get("http://ata123456789123456789.appspot.com/")
#     atacalcpage = ataCalcPage(driver)
#
#     n1 = 12
#     n2 = 15
#     expResult = 27
#     print(n1,' ',n2,' ',)
#
#     atacalcpage = ataCalcPage(driver)
#     actResult = atacalcpage.add(n1,n2)
#     print(actResult)
#
#     assert expResult == actResult
#
# def test_Calc_mul():
#     driver.get("http://ata123456789123456789.appspot.com/")
#     atacalcpage1 = ataCalcPage(driver)
#
#     n1 = 12
#     n2 = 10
#     expResult = 120
#     print(n1,' ',n2,' ',)
#
#     atacalcpage1 = ataCalcPage(driver)
#     actResult = atacalcpage.add(n1,n2)
#     print(actResult)