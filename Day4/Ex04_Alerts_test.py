from selenium import webdriver


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.get('https://the-internet.herokuapp.com/javascript_alerts')
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('Teardonw Method()')
    driver.quit()

def test_validate_JSAlert() :
    print('Inside test validate JS alert() method')
    first = driver.find_element_by_xpath("//*[@id='content']/div/ul/li[1]/button")
    first.click()

    alert1 = driver.switch_to.alert
    alert1.accept()
    expResult = "You successfuly clicked an alert"

    actResult = driver.find_element_by_xpath("//*[@id='result']").text
    print(expResult)
    print(actResult)

    if(expResult == actResult):
        print("You successfully clicked on alert")

def test_validate_JSConfirm_Ok() :
    print('Confirming JSConfirm()')
    second = driver.find_element_by_xpath("//*[@id='content']/div/ul/li[2]/button")
    second.click()

    alert2 = driver.switch_to.alert
    alert2.accept()
    expResult2 = "You clicked: Ok"

    actResult2 = driver.find_element_by_xpath("//*[@id='result']").text
    # print(expResult2)
    # print(actResult2)
    # alert3 = driver.switch_to.alert
    # alert3.dismiss()

    # expResult3 = "You clicked: Cancel"
    # print("You clicked: Cancel")

    assert expResult2 == actResult2



def test_validate_JSConfirm_Cancel() :
    print('Confirming JSConfirm()')
    second = driver.find_element_by_xpath("//*[@id='content']/div/ul/li[2]/button")
    second.click()

    alert2 = driver.switch_to.alert
    # alert2.accept()
    # expResult2 = "You clicked: OkK"
    #
    # actResult2 = driver.find_element_by_xpath("//*[@id='result']").text
    # # print(expResult2)
    # print(actResult2)
    alert3 = driver.switch_to.alert
    alert3.dismiss()

    expResult3 = "You clicked: Cancel"
    actResult3 = driver.find_element_by_xpath("//*[@id='result']").text
    print("You clicked: Cancel")

    assert expResult3 == actResult3




