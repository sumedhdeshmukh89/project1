#import time
import time

from selenium import webdriver
from selenium.webdriver.support.select import Select
def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()

    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print('Teardown Method()')
    time.sleep(3)
    driver.quit()

def test_verify_dropown():
    print('Inside test_verify_dropown() method')
    driver.get('file:///C:/Users/sumedh_deshmukh/PycharmProjects/Practice/resources/data/dropdown.html')

    # /html/body/form/select
    dd = driver.find_element_by_xpath("/html/body/form/select")
    selectObject = Select(dd)

    lstOptions = selectObject.options

    for opt in lstOptions:
        print(opt.text)

    # time.sleep(3)
    # selectObject.select_by_visible_text("CP-AAT")

    time.sleep(3)
    selectObject.select_by_value("7")
    
    driver.switch_to.frame("Java")


