from selenium import webdriver
from selenium.webdriver import ActionChains

def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.get('https://www.annauniv.edu/department/index.php')
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('Teardonw Method()')
    driver.quit()


def test_validate_iom():
    print('Inside test_validate_iom()  method')
    action = ActionChains(driver)
    driver.get('https://www.annauniv.edu/department/index.php')
    link2 = driver.find_element_by_xpath("//*[@id=\"link3\"]/strong")
    OceanMgmt = driver.find_element_by_xpath("//*[@id=\"menuItemHilite32\"]")
    action.move_to_element(link2).move_to_element(OceanMgmt).click().perform()
    print(driver.title)

