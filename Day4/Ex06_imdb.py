import pytest
from selenium import webdriver


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()
    driver.get('https://www.imdb.com/')
    driver.maximize_window()
    driver.implicitly_wait(10)

def teardown():
    print('Teardonw Method()')
    driver.quit()

def dataGenerator():
    lst = ['Raazi','Meghana Gulzar','Vicky'],['Baazigar','Sholey','TimeMachine']
    return  lst

@pytest.mark.parametrize('data',dataGenerator())
def test_search(data):
    movie = data[0]
    expDir =data[1]
    expStar = data[2]
    print(data)

    search = driver.find_element_by_xpath("//*[@id='suggestion-search']")
    search.send_keys(movie)

    icon = driver.find_element_by_xpath("//*[@id='suggestion-search-button']")
    icon.click()
    movie = driver.find_element_by_xpath("//*[@id='main']/div/div[2]/table/tbody/tr[1]/td[2]/a")
    movie.click()

    lstStars = driver.find_elements_by_xpath("//*[@class='inline' and contains(text(),'Star')]/following-sibling::*")

    starFound = False
    for Star in lstStars:
        starname = Star.text
        print(starname)
        if( expStar in starname):
            starFound = True
        print('Found the Star', expStar, ' :::: ', starname)
        break

