import pytest
from selenium import webdriver
import time


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()

    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print('Teardonw Method()')
    time.sleep(3)
    driver.quit()

def test_calc_multiply():
    driver.get("http://ata123456789123456789.appspot.com/")
    num1 = 10
    num2 = 20
    expResult = 200


    input1 = driver.find_element_by_xpath("//*[@id=\"ID_nameField1\"]")
    input1.clear()
    input1.send_keys(str(num1))

    input2 = driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")
    input2.clear()
    input2.send_keys(str(num2))

    mulradio = driver.find_element_by_xpath("//*[@id=\"gwt-uid-2\"]")
    mulradio.click()

    #  //*[@id="ID_calculator"]    calcluate button

    calcbtn = driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
    calcbtn.click()

    result = driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")
    actResult = int(result.get_attribute('value'))

    print('actResult :',actResult,'expResult:',expResult )

    assert expResult == actResult

def test_calc_add():
    driver.get("http://ata123456789123456789.appspot.com/")
    num1 = 10
    num2 = 20
    expResult1 = 30

    input1 = driver.find_element_by_xpath("//*[@id=\"ID_nameField1\"]")
    input1.clear()
    input1.send_keys(str(num1))

    input2 = driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")
    input2.clear()
    input2.send_keys(str(num2))

    addradio = driver.find_element_by_xpath("//*[@id=\"gwt-uid-1\"]")
    addradio.click()

    calcbtn1 = driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
    calcbtn1.click()

    result1 = driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")
    actResult1 = int(result1.get_attribute('value'))

    print('actResult1 :', actResult1, 'expResult1:', expResult1)

    assert expResult1 == actResult1