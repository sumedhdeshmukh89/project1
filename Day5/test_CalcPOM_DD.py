import pytest
from selenium import webdriver
from Day5.CalcPOM import ataCalcPage
import time


def setup():
    print('Inside setup() method')
    global driver
    driver = webdriver.Chrome()

    driver.maximize_window()
    driver.implicitly_wait(10)


def teardown():
    print('Teardonw Method()')
    time.sleep(3)
    driver.quit()

def dataprovider():
    lol = [ [12,  13,  'Add',  25],
            [10,  20,  'Mul',  200],
            [18,  32,  'Add',  50],
            [500, 100, 'comp',  500],
            [50,  12,  'add',  62]   ]
    return lol


@pytest.mark.parameterize('inputdata', dataprovider())
def test_calc_all(inputdata):
    print(inputdata)
    input1 = inputdata[0]
    input2 = inputdata[1]
    operation = inputdata[2]
    expRes = inputdata[3]
    actRes = ''
    atacalcpage = ataCalcPage(driver)

    if(operation.upper() == 'ADD'):
        actRes = atacalcpage.add(input1, input2)
    elif (operation.upper() == 'MUL'):
        actRes = atacalcpage.multiply(input1, input2)
    elif (operation.upper() == 'COMP'):
        actRes = atacalcpage.comp(input1, input2)
    else:
        print('Invalid Data')

    print(expRes, actRes)
    assert expRes == actRes




# def test_Calc_add():
#     driver.get("http://ata123456789123456789.appspot.com/")
#     atacalcpage = ataCalcPage(driver)
#
#     n1 = 12
#     n2 = 15
#     expResult = 27
#     print(n1,' ',n2,' ',)
#
#     atacalcpage = ataCalcPage(driver)
#     actResult = atacalcpage.add(n1,n2)
#     print(actResult)
#
#     assert expResult == actResult
#
# def test_Calc_mul():
#     driver.get("http://ata123456789123456789.appspot.com/")
#     atacalcpage1 = ataCalcPage(driver)
#
#     n1 = 12
#     n2 = 10
#     expResult = 120
#     print(n1,' ',n2,' ',)
#
#     atacalcpage1 = ataCalcPage(driver)
#     actResult = atacalcpage.add(n1,n2)
#     print(actResult)