from selenium import webdriver
import time

class ataCalcPage:

    def __init__(self,driverobj):
        self.driver = driverobj


        self.input1 = self.driver.find_element_by_xpath("//*[@id=\"ID_nameField1\"]")
        self.input2 = self.driver.find_element_by_xpath("//*[@id=\"ID_nameField2\"]")
        self.addradio = self.driver.find_element_by_xpath("//*[@id='gwt-uid-1']")
        self.multiply = self.driver.find_element_by_xpath("//*[@id=\"gwt-uid-2\"]")
        self.compradio = self.driver.find_element_by_xpath("//*[@id='gwt-uid-4']")
        self.calc = self.driver.find_element_by_xpath("//*[@id=\"ID_calculator\"]")
        self.resultBox = self.driver.find_element_by_xpath("//*[@id=\"ID_nameField3\"]")
        self.Euclid = self.driver.find_element_by_xpath("//*[@id=\"gwt-uid-6\"]")
        #self.comp = self.driver.find_element_by_xpath("//*[@id=\"gwt-uid-4\"]")


    def add(self, num1, num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.addradio.click()

        self.calc.click()

        actRes = int(self.resultBox.get_attribute('value'))

        return actRes

    def Mul(self, num1, num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.multiply.click()

        self.calc.click()

        actRes = int(self.resultBox.get_attribute('value'))

        return actRes

    def comp(self, num1,  num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.compradio.click()

        self.calc.click()

        actRes = int(self.resultBox.get_attribute('value'))

        return actRes

    def euc(self, num1,  num2):
        self.input1.clear()
        self.input1.send_keys(str(num1))

        self.input2.clear()
        self.input2.send_keys(str(num2))

        self.Euclid.click()

        self.calc.click()

        actRes = int(self.resultBox.get_attribute('value'))

        return actRes

