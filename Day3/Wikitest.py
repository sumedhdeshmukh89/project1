import time
import unittest
from selenium import webdriver



class MyTestCase(unittest.TestCase):


    def setUp(self):
        print('entered setup()')
        global driver
        driver = webdriver.Chrome()
        driver.maximize_window()
        driver.implicitly_wait(10)

    def tearDown(self):
        print('entered teardown()')
        time.sleep(3)
        driver.quit()


    def testWiki(self):
        print('enter testwiki()')
        driver = webdriver.Chrome()
        driver.maximize_window()
        driver.get('https://www.wikipedia.org/')
        driver.save_screenshot('../screenshots/wikiPage1.png')
        pageTitle = driver.title
        print(pageTitle)

        # "//*[@id=\"js-link-box-en\"]/strong"

        # eng = driver.find_element_by_xpath("//*[@id=\"js-link-box-en\"]/strong")
        # eng.click()
        eng = driver.find_element_by_id("js-link-box-en")
        eng.click()

        print('Title of Page 2', driver.title)
        driver.save_screenshot('../screenshots/wikiPage2.png')
        search = driver.find_element_by_xpath("//*[@id=\"searchInput\"]")
        search.send_keys('Selenium')
        search = driver.find_element_by_name("search")
        searchbar = driver.find_element_by_xpath("//*[@id=\"searchButton\"]").click()
        print('Title of Page 3', driver.title)
        # driver.save_screenshot('../screenshots/wikiPage3.png')

        self.assertEqual('Selenium - Wikipedia', driver.title)
       # assert driver.title == 'Selenium - Wikipedia'
        driver.back()
        print('Title of page 3:', driver.title)
        driver.forward()
        print('Title of page currentpage:', driver.title)

        time.sleep(2)
        driver.close()






    #
    # def test_something(self):
    #     self.assertEqual(True, False)


# if __name__ == '__main__':
#     unittest.main()
