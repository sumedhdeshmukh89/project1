from Day3.Classes.Personclass import Person

class Student(Person):

    def __init__(name, age, std):
        super().__init__(name, age)
        self.std = std


    def greetings(self):
        print('Greeting from :', self.name)

st1 = Student('Sumedh', 32, 'VI')

print(st1.name, st1.age, st1.std)
