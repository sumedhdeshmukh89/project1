class Person:

    def __init__(self,name,age):
        self.name = name
        self.age = age

    def SayHello(self):
        print("HI Everyone my name is:", self.name)


P1 = Person('Sumedh', 32)
print(P1.name, P1.age)
P2 = Person('Bageshri', 27)
print(P2.name, P2.age)


P1.SayHello()
P2.SayHello()
