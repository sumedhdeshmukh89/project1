import unittest
from Day3.Classes.calculator import Calc

class MyTestCase(unittest.TestCase):
    def test_validateAdd(self):
        print('Inside test_validateAdd() method')
        exp1 = 30
        res1 = Calc.add(self,10,20)
        self.assertEqual(exp1,res1,"Failed due to mismatch")
        # Calc.add()

    def test_mult(self):
        print('Inside test_mult() method')
        self.assertEqual(100, Calc.mul(self, 10, 10))


# if __name__ == '__main__':
#     unittest.main()
