from selenium import webdriver
import time

driver = webdriver.Chrome()
driver.maximize_window()

driver.get('https://www.wikipedia.org/')
driver.save_screenshot('../screenshots/wikiPage1.png')
pageTitle = driver.title
print(pageTitle)

# "//*[@id=\"js-link-box-en\"]/strong"

# eng = driver.find_element_by_xpath("//*[@id=\"js-link-box-en\"]/strong")
# eng.click()
eng = driver.find_element_by_id("js-link-box-en")
eng.click()

print('Title of Page 2', driver.title)
# driver.save_screenshot('../screenshots/wikiPage2.png')
# search = driver.find_element_by_xpath("//*[@id=\"searchInput\"]")
# search.send_keys('Selenium')
search = driver.find_element_by_name("search")
searchbar = driver.find_element_by_xpath("//*[@id=\"searchButton\"]").click()
print('Title of Page 3', driver.title)
# driver.save_screenshot('../screenshots/wikiPage3.png')
assert driver.title =='Selenium - Wikipedia'
driver.back()
print('Title of page 3:', driver.title)
driver.forward()
print('Title of page currentpage:', driver.title)

time.sleep(2)
driver.close()